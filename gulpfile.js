var Promise = require('es6-promise').Promise;
var gulp = require('gulp');
var sass = require('gulp-sass');
var cssnano = require('gulp-cssnano');
var autoprefixer = require('gulp-autoprefixer');

gulp.task('default', function(){
    gulp.src('public/css/style.scss')
    .pipe(sass())
    .pipe(autoprefixer())
    .pipe(cssnano())
    .pipe(gulp.dest('build/css'));
});

gulp.task('watch', function() {
    gulp.watch('public/css/*.scss', ['default']);
});

