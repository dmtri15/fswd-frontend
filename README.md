##Synopsis

This is the front end portfolio project from Udacity's Full Stack Web
Development nanodegree course.
## Installation

Please download the zip file or clone the project at: [project
link](https://bitbucket.org/dmtri15/fswd-frontend)

then `cd` into the project and run the following line in the command line (with
NodeJS and npm installed):

```npm install```

```gulp```

(you might need to install gulp and gulp-cli globally first to run gulp
command)

## Contributors

If you catch any bug, feel free to open a new pull request or issue

## License

MIT license